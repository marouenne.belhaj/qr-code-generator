import qrcode
from qrcode.image.styledpil import StyledPilImage
from qrcode.image.styles.moduledrawers.pil import RoundedModuleDrawer, SquareModuleDrawer, CircleModuleDrawer, HorizontalBarsDrawer
from qrcode.image.styles.colormasks import RadialGradiantColorMask, SolidFillColorMask, SquareGradiantColorMask, HorizontalGradiantColorMask, ImageColorMask
from PIL import Image, ImageDraw, ImageOps
import numpy as np
import crypto
from cryptography.fernet import Fernet


def generate_qr_code(content, qr_file, logo_file, output_file):
    qr = qrcode.QRCode(
        version=None,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=10,
        border=4,
    )

    key = Fernet.generate_key()
    encrypted_content = crypto.encrypt_data(content, key)
    qr.add_data(encrypted_content)
    qr.make(fit=True)

    img = qr.make_image(image_factory=StyledPilImage, module_drawer=RoundedModuleDrawer(), color_mask=RadialGradiantColorMask())

    # Gradient color effect
    width, height = img.size
    gradient = Image.linear_gradient("L").resize(img.size)
    gradient_map = np.array(gradient).mean(axis=-1) / 255

    # Add logo
    logo = Image.open(logo_file)
    logo.thumbnail((width // 4, height // 4))
        # Ensure the logo image has an alpha channel
    if logo.mode != 'RGBA':
        logo = logo.convert('RGBA')

    logo_w, logo_h = logo.size
    x = (width - logo_w) // 2
    y = (height - logo_h) // 2

    img.paste(logo, (x, y), mask=logo)

    # Save the output image
    img.save(output_file)

if __name__ == "__main__":
    content = "https://example.com"
    qr_file = "/workspace/qr-code-generator/static/images/qr_code.png"
    logo_file = "/workspace/qr-code-generator/static/images/logo.jpg"
    output_file = "/workspace/qr-code-generator/static/images/QR-Code.png"

    generate_qr_code(content, qr_file, logo_file, output_file)
